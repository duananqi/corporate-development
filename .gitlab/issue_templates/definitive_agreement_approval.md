## Project [project_code_name] Final Deal Approvals

** Please DO NOT include any deal specific confidential details such as payment terms, milestones and more **

* Main collaboration document [insert_hyperlink]
* Definitive agreement (APA or otherwise) [insert_hyperlink] - please refer to this document for final deal terms

The purpose of this issue is to track approvals to sign the definitive acquisition agreement.

## Final approvals for signing

* [ ]  CEO
* [ ]  CFO
* [ ]  CLO
* [ ]  EVP Product
    * [ ]  Product Champion
* [ ]  EVP Engineering
    * [ ]  Engineering Champion
* [ ]  Dir. Corp Dev